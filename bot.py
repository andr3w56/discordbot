import discord
from discord.ext import commands

import select_menu

# Se refiere a los diferentes permisos y a que va a acceder el bot.
intents = discord.Intents.all()
# En este caso se va a trabajar con mensajes.
intents.messages = True
# Se define el prefijo para el cual el bot va a escuchar los mensajes.
client = commands.Bot(command_prefix='!', intents=intents)


# Ya no se utiliza la clase Client porque se va a trabajar con comandos personalizados.
# client = discord.Client(intents=intents)


@client.event
async def on_ready():
    print('El bot está listo!')


@client.command()
async def saludo(ctx):
    await ctx.send('Hola!')


@client.command()
async def embedprueba(ctx):
    embed = discord.Embed(title='kuspi',
                          type='image', color=0xFF0000)
    embed.set_image(url='https://media.discordapp.net/stickers/945504221267128340.png?size=160')
    await ctx.send(embed=embed)


@client.command()
async def devin(ctx):
    embed = discord.Embed(title='devin', description='devin')
    embed.set_image(url='https://cdn.discordapp.com/attachments/1052651229131571230/1058143079183417384/STK-20221108'
                        '-WA0014.webp')
    await ctx.send(embed=embed)


@client.command()
async def tiene_rol(ctx):
    rol_prueba = 'rol-prueba'
    try:
        roles_usuario = [r.name for r in ctx.message.author.roles]
        if rol_prueba not in roles_usuario:
            raise commands.errors.MissingRole(format('<@&1058145297353683067>'))
        await ctx.send('El usuario tiene el rol <@&1058145297353683067>')
    except commands.errors.MissingRole as ex:
        await ctx.send(format(ex))


@client.command()
async def menu(ctx):
    select = select_menu.MenuSelect()
    await ctx.send(view=select)


client.run('MTA1MzgzOTY1OTg0NDc3MTg5MA.Ggbo31.8nYLSbLrqAgcOPiJ3cxUSp0vZDU0V_4Q0qnPtc')
