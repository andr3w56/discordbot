import discord
from discord.ui import View


class MenuSelect(View):
    @discord.ui.select(
        placeholder='Elije una opción',
        options=[
            discord.SelectOption(label="Prueba",
                                 emoji="✔",
                                 value='1',
                                 description='Prueba de menus'),
            discord.SelectOption(label="Mensaje",
                                 emoji="📩",
                                 value='2',
                                 description='Envia un mensaje')
        ]
    )
    async def select_callback(self, interaction, select):
        select.disabled = True
        if select.values[0] == '1':
            return
        if select.values[0] == '2':
            await interaction.response.send_message("Hola")
