# This is a sample Python script.
import bot


def print_hi(name):
    print(f'Hi, {name}')



if __name__ == '__main__':
    print_hi('Starting discord.py bot...')
    # Importamos la librería y el módulo necesarios para trabajar con comandos personalizados.
    import discord

    # Se refiere a los diferentes permisos y a que va a acceder el bot.
    intents = discord.Intents.all()
    # En este caso se va a trabajar con mensajes.
    intents.messages = True
    # Se define el prefijo para el cual el bot va a escuchar los mensajes.
    client = bot.client
